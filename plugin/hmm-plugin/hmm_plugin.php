<?php

/**
 * Plugin Name: Hmm_plugin
 * Plugin URI: 
 * Description: Plugin HMM. CMS PROJECT.
 * Version: v1 Beta
 * Author: G4
 * Author URI: O.S.E.F
 * License: OPENSOURCE
 */
// FICHIER DE BASE DU PLUGIN
if ( !defined('ABSPATH') ) //stop l'include si pas utilisé
	die(-1);

//plugin tables
global $wpdb;
$wpdb->tables[] = 'posts'; // definir les tables à utiliser
$wpdb->name_table = $wpdb->prefix . "esgi"; // prefix

// les constantes
define('DEV_URL', plugin_dir_url(__FILE__)); // url du développement
define('DEV_DIR', plugin_dir_path(__FILE__)); // répertoire
define('DEV_VERSION', '0.1'); // numero de version
define('DEV_NAME', 'Search'); // le nom du plugin
define('DEV_OPTION_SETTING', 'search-settings'); // les différentes options
// fonction pour inclure/charger facilement les fichiers

function _search_load_files($dir, $files, $prefix = '') {
    foreach ($files as $file) {
        // echo $dir . $prefix . $file . ".php <br>";
        if (is_file($dir . $file . ".php")) {
            require_once($dir . $file . ".php");
        }
    }
}

// les class clientes
_search_load_files(DEV_DIR, array('Search_class', 'Filter_class'));

// hook - fonction appelé lors de l'activation et lors de la désactivation du plugin
// register_activation_hook();
// register_desactivation_hook();

function init_hmm_plugin() 
{

    global $hmmPlugin;

    //charge class pour le client | pas de d'administration
    $hmmPlugin['client'] = new HmmSearchFilter();
}

add_action('plugin_loaded', 'init_hmm_plugin');

function get_searchfilter($atts) 
{
    global $hmmPlugin, $wpdb;

    $wo_searching = 0; // initialisation à 0

    $hmmSettings = new HmmSettings();
}
