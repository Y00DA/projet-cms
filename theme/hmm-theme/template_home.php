<?php /* Template Name: Page d'accueil */
get_header();

while (have_posts()):
    $bannerImage = get_the_post_thumbnail_url();
    $bannerTitle = get_post_meta(get_the_ID(), 'banner_title', true);
    $bannerLead = get_post_meta(get_the_ID(), 'banner_text', true);

    the_post();
    if (has_post_thumbnail()):
        ?>
        <div id="cover" style="background-image: url('<?php echo $bannerImage; ?>');">
            <div class="container">
                <div id="cover-title">
                    <h1><?php echo empty($bannerTitle) ? bloginfo('name') : $bannerTitle; ?></h1>
                    <p class="lead"><?php echo empty($bannerLead) ? bloginfo('description') : $bannerLead; ?></p>
                </div>
                <div id="cover-arrow">
                    <a href="#page_content"><span class="glyphicon glyphicon-triangle-bottom"></span></a>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div id="page_content">
        <?php the_content(); ?>
    </div>
    <?php
endwhile;

get_search_form();

get_footer();