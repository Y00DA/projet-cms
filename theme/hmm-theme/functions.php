<?php

require get_template_directory() . '/shortcode.php';
require get_template_directory() . '/function/templates.php';

/**
 * Proper way to enqueue scripts and styles
 */
function hmm_theme_name_scripts()
{
    wp_enqueue_style('bootstrap-style', get_template_directory_uri() . '/vendor/css/bootstrap.min.css');
    wp_enqueue_style('bootstrap-theme', get_template_directory_uri() . '/vendor/css/bootstrap-theme.min.css');
    wp_enqueue_style('hmm-theme-style', get_stylesheet_uri());
    wp_enqueue_style('hmm-theme-css', get_template_directory_uri() . '/css/style.css');
    wp_enqueue_script('bootstrap-script', get_template_directory_uri() . '/vendor/js/bootstrap.min.js', array(), '1.0.0', true);
    wp_enqueue_script('hmmm-main-script', get_template_directory_uri() . '/js/main.js', array(), '1.0.0', true);
}

/** Theme widgets * */
function hmm_theme_widgets()
{
    require get_template_directory() . '/function/copyrightWidget.class.php';
    register_widget('CopyrightWidget');
}

/** Navigation menus and sidebar * */
function hmm_theme_menus()
{
    register_nav_menus(array(
        'main_menu' => 'Menu de navigation',
        'footer_menu' => 'Menu de pied de page'
    ));
}

function hmm_theme_sidebars()
{
    register_sidebar(
        array(
            'id' => 'widget_header',
            'name' => __('Widgets d\'en-tête', 'hmm'),
            'class' => '',
            'description' => '',
            'before_widget' => '<div class="widget">',
            'after_widget' => '</div>',
            'before_title' => '<h2>',
            'after_title' => '</h2>'
        ));
    register_sidebar(
        array(
            'id' => 'widget_footer',
            'name' => __('Widgets de pied de page', 'hmm'),
            'class' => 'footer-widgets',
            'description' => '',
            'before_widget' => '<div class="widget">',
            'after_widget' => '</div>',
            'before_title' => '<h2>',
            'after_title' => '</h2>'
        )
    );
}

/** Theme settings * */
function hmm_theme_register_settings()
{
    register_setting('hmm-theme', 'hmm-header-color');
    register_setting('hmm-theme', 'hmm-transparent-header');
    register_setting('hmm-theme', 'hmm-maps-api-key');
    register_setting('hmm-theme', 'hmm-slider');
}

/** Theme option pages * */
function hmm_theme_register_menu_pages()
{
    add_menu_page('Options du thème', 'Options thème', 'administrator', 'manage_options', 'hmm_theme_option_page');
}

function hmm_theme_slider_option_page()
{
    require get_template_directory() . '/slider-options-page.php';
}

function hmm_theme_option_page()
{
    require get_template_directory() . '/options-page.php';
}

function hmm_theme_recettes()
{
    register_post_type('recette', array(
        'label' => __('Recettes'),
        'singular_label' => __('Recette'),
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
    ));
}

/**
 * Register meta box(es).
 */
function hmm_theme_register_meta_boxes()
{
    add_meta_box('meta-box-id', __('Bandeau d\'en-tête', 'textdomain'), 'hmm_theme_meta_box_display_callback', 'page');
}

/**
 * Save meta box content.
 *
 * @param int $post_id Post ID
 */
function hmm_theme_save_meta_box($post_id)
{
    foreach (['banner_title', 'banner_text'] as $field) {
        if (isset($_POST[$field])) {
            update_post_meta($post_id, $field, esc_html($_POST[$field]));
        }
    }
}

/** Theme style customization **/
function hmm_theme_head_style()
{
    echo '<style>';
    echo 'header { 
        background: ' . get_theme_mod('header_background_color', '#6a121a') . ';' .
        'color: ' . get_theme_mod('header_text_color', '#fff') . ';' .
        '}';
    echo '</style>';
}

function hmm_theme_customize_register($wp_customize)
{
    $wp_customize->add_setting('header_background_color', array(
        'default' => '#6a121a',
        'transport' => 'refresh',
    ));

    $wp_customize->add_setting('header_text_color', array(
        'default' => '#ffffff',
        'transport' => 'refresh',
    ));

    $wp_customize->add_setting('footer_background', array(
        'default' => '',
        'transport' => 'refresh',
    ));

    $wp_customize->add_setting('footer_text_color', array(
        'default' => '#ffffff',
        'transport' => 'refresh',
    ));

    $wp_customize->add_section('hmm_theme_section_header', array(
        'title' => __('Couleurs d\'en-tête', 'hmm-theme'),
        'priority' => 30,
    ));

    $wp_customize->add_section('hmm_theme_section_footer', array(
        'title' => __('Couleurs du pied de page', 'hmm-theme'),
        'priority' => 30,
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'header_background_color', array(
        'label' => __('Couleur de fond', 'hmm-theme'),
        'section' => 'hmm_theme_section_header',
        'settings' => 'header_background_color',
    )));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'header_text_color', array(
        'label' => __('Couleur de texte', 'hmm-theme'),
        'section' => 'hmm_theme_section_header',
        'settings' => 'header_text_color',
    )));

    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'footer_background', array(
        'label'    => __( 'Fond du pied de page', 'footer_background' ),
        'section'  => 'hmm_theme_section_footer',
        'settings' => 'footer_background',
    ) ) );
}

define('HMM_THEME_HOME_TEMPLATE_PAGE_SLUG', 'template_home.php');

/** WP Hooks * */
add_action('init', 'hmm_theme_menus');
add_action('admin_init', 'hmm_theme_register_settings');
add_action('widgets_init', 'hmm_theme_sidebars');
add_action('widgets_init', 'hmm_theme_widgets');
add_action('admin_menu', 'hmm_theme_register_menu_pages');
add_action('wp_enqueue_scripts', 'hmm_theme_name_scripts');
add_action('wp_head', 'hmm_theme_head_style');
add_action('init', 'hmm_theme_recettes');
add_action('add_meta_boxes', 'hmm_theme_register_meta_boxes');
add_action('save_post', 'hmm_theme_save_meta_box');
add_action('customize_register', 'hmm_theme_customize_register');
define('NO_HEADER_TEXT', true);
add_theme_support('custom-header');
add_theme_support('post-thumbnails');
