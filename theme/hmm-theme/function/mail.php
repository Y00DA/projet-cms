<?php

function hmm_theme_handle_mail_form()
{
    $name = htmlentities(trim($_POST['username']));
    $phone = htmlentities(trim($_POST['phone']));
    $message = htmlentities(trim($_POST['message']));
    $from = filter_var($_POST['mail'], FILTER_VALIDATE_EMAIL);
    if ($from == false) {
        return ['error' => 'L\'adresse email fournie est incorrecte'];
    }

    wp_mail(
        get_option('admin_email'),
        "Nouveau message de " . $name . " : " . $from,
        (!empty($phone) ? "Téléphone: " . $phone : '') . PHP_EOL .
        "Message : " . PHP_EOL . $message
    );

    return true;
}