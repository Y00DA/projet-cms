<?php

class CopyrightWidget extends WP_Widget
{
    function CopyrightWidget()
    {
        $options = array(
            'classname' => 'copyright-widget',
            'description' => 'Widget permettant d\'afficher une mention de copyright ainsi qu\'un lien.'
        );
        parent::__construct('copyright-widget', 'Copyright', $options);
    }

    function widget($args, $instance)
    {
        echo '<p>&copy; ' . date('Y') . ' ' . $instance['site_name'] . '<br>';
        echo '<a href="' . $instance['producer_url'] . '">Réalisation ' . $instance['producer_name'] . '</a></p>';
    }

    function update($new_instance, $old_instance)
    {
        return $new_instance;
    }

    private function field($instance, $label, $fieldName)
    {
        return '<label for="' . $this->get_field_id($fieldName) . '">' . $label . '</label>'
        . '<input name="' . $this->get_field_name($fieldName) . '" '
        . 'id="' . $this->get_field_id($fieldName) . '"'
        . 'value="' . $instance[$fieldName] . '"'
        . ' type="text" />';
    }

    function form($instance)
    {
        $params = [
            'site_name' => get_bloginfo('name'),
            'producer_name' => 'ESGI',
            'producer_url' => 'http://esgi.fr'
        ];

        $instance = wp_parse_args($instance, $params);

        echo $this->field($instance, 'Détenteur des droits : ', 'site_name') . '<br>' .
            $this->field($instance, 'Réalisateur : ', 'producer_name') . '<br>' .
            $this->field($instance, 'URL Réalisateur : ', 'producer_url') . '<br>';
    }
}