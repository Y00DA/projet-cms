<?php

function hmm_theme_page_banner()
{
    $bannerImage = get_the_post_thumbnail_url();
    $bannerTitle = get_post_meta(get_the_ID(), 'banner_title', true);
    $displayBanner = !empty($bannerImage) && !empty($bannerTitle);

    if (has_post_thumbnail()) {
        echo <<<EOT
    <div id="cover" class="small" style="background-image: url('$bannerImage');">
        <div class="container">
            <div id="cover-title">
                <h1>$bannerTitle</h1>
            </div>
        </div>
    </div>
EOT;
    }

    return $displayBanner;
}

/**
 * Meta box display callback.
 *
 * @param WP_Post $post Current post object.
 */
function hmm_theme_meta_box_display_callback($post)
{
    $currentTitle = esc_html(get_post_meta($post->ID, 'banner_title', true));

    echo <<<EOT
<p>Pour ajouter un bandeau sur cette page, ajoutez une image en cliquant sur le bouton présent dans la boîte
"Image à la Une" sur la droite de cette page d'édition. Laissez l'image vide pour ne pas avoir de bandeau.</p>

<table class="form-table">
    <tr>
        <th scope="row">
            <label for="banner_title">Texte du bandeau</label>
        </th>
        <td>
            <input id="banner_title" type="text" name="banner_title" value="$currentTitle" />
        </td>
    </tr>
EOT;
    // if the page is using the home page template
    if (get_page_template_slug($post->ID) == HMM_THEME_HOME_TEMPLATE_PAGE_SLUG ) {
        $currentText = esc_html(get_post_meta($post->ID, 'banner_text', true));
        echo <<<EOT
    <tr>
        <th scope="row">
            <label for="banner_text">Slogan</label>
        </th>
        <td>
            <input id="banner_text" type="text" name="banner_text" value="$currentText" />
        </td>
    </tr>
EOT;

    }

    echo <<<EOT
        </table>
    <p>
</p>
EOT;


}
