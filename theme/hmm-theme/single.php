<?php
get_header();
?>

<!--    <div id="cover">-->
<!--        <div class="container">-->
<!--            <div id="cover-title">-->
<!--                <h1>--><?php //echo bloginfo('name');    ?><!--</h1>-->
<!--                <p class="lead">--><?php //echo bloginfo('description');    ?><!--</p>-->
<!--            </div>-->
<!--            <div id="cover-arrow">-->
<!--                <a href="#about"><span class="glyphicon glyphicon-triangle-bottom"></span></a>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->

<?php while (have_posts()) : ?>
    <div class="container site-post">
        <?php the_post(); ?>
        <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>

        <div class="post-content">
            <?php
            the_content();
            ?>
        </div>

    </div>
<?php endwhile; ?>

<?php
get_footer();
