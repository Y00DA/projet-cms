<?php
get_header();

while (have_posts()) :
    $displayBanner = hmm_theme_page_banner();
    ?>
    <div class="container site-post">
        <?php the_post(); ?>
        <?php if (!$displayBanner): ?>
            <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
        <?php endif; ?>
        <div class="post-content no-title">
            <?php the_content(); ?>
        </div>
    </div>
<?php endwhile; ?>

<?php get_search_form(); ?>

<?php
get_footer();
