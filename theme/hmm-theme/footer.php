</div> <!-- /#page -->
<footer id="footer">
    <div class="container">
        <nav class="navbar">
            <?php
            if (has_nav_menu('footer_menu')) {
                wp_nav_menu(array('theme_location' => 'footer_menu',
                    'container' => '',
                    'menu_class' => 'nav navbar-nav list-inline',
                    'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>'));
            }
            ?>
            <div class="navbar-text navbar-right">
                <?php dynamic_sidebar('widget_footer'); ?>
            </div>
        </nav>
    </div>
</footer>

<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
        integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
        crossorigin="anonymous"></script>
<?php wp_footer(); ?>
</body>
</html>