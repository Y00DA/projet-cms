<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width">
    <title><?php bloginfo('title'); wp_title('-'); ?></title>
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header id="page_header" <?php echo ((is_home() || is_front_page()) && get_option('hmm-transparent-header') == "on") ? 'class="autoTransparent transparent"' : ''; ?>>
    <div class="container">
        <nav class="navbar">
            <a href="#navbar" data-toggle="collapse" data-target="#navbar"
               class="pull-right visible-xs navbar-toggle collapsed">
                <span class="glyphicon glyphicon-menu-hamburger"></span>
            </a>
            <div class="navbar-header">
                <div id="logo">
                    <?php if (get_header_image()) : ?>
                        <a href="<?php echo esc_url(home_url()); ?>"
                           class="logo"><img src="<?php header_image(); ?>" alt="logo"/></a>
                    <?php else: ?>
                        <a class="navbar-brand"
                           href="<?php echo esc_url(home_url()); ?>"><?php bloginfo('name'); ?></a>
                    <?php endif; ?>
                </div>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <?php
                if (has_nav_menu('main_menu')) {
                    wp_nav_menu(array('theme_location' => 'main_menu',
                        'container' => '',
                        'menu_class' => 'nav navbar-nav',
                        'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>'));
                }
                ?>
            </div>
        </nav>
    </div>
</header>
<!---->
<!--<div id="sidebar">-->
<!--    --><?php //dynamic_sidebar('widget_left'); ?>
<!--</div>-->
<div id="page">
    <!--    <div id="sidebar" class="sidebar">-->
    <!--        <header id="masthead" class="site-header" role="banner">-->
    <!--            <div class="site-branding">-->
    <!--                <h1 class="site-title"><a href="--><?php //echo esc_url(home_url('/')); ?><!--"-->
    <!--                                          rel="home">--><?php //bloginfo('name'); ?><!--</a></h1>-->
    <!--                <p class="site-description">--><?php //echo get_bloginfo('description'); ?><!--</p>-->
    <!--            </div>-->
    <!--        </header>-->
    <!---->
    <!--        --><?php //get_sidebar(); ?>
    <!--    </div><!-- .sidebar - ->-->

    <div id="content" class="site-content">
