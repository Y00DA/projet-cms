<?php

function hmm_theme_shortcode_section($atts, $content = null) {
    $a = shortcode_atts(array(
        'class' => 'base',
            ), $atts);

    $class = esc_attr($a['class']);
    $content = do_shortcode($content);
    $html = <<<EOT
	<section class="$class">
		<div class="container">$content</div>
	</section>
EOT;

    return $html;
}

function hmm_theme_shortcode_container($atts, $content = null) {
    return '<div class="container">' . do_shortcode($content) . '</div>';
}

function hmm_theme_shortcode_map($atts, $content = null) {
    $a = shortcode_atts(array(
        'address' => '',
            ), $atts);

    return '<section class="map" data-google-address="' . trim($a['address']) . '" data-gmap-api-key="' . trim(get_option('hmm-maps-api-key')) . '"><div class="content">'
            . do_shortcode($content) . '</div></section>';
}

function hmm_theme_shortcode_recettes() {
    require get_template_directory() . '/recettes.php';
}

add_shortcode('section', 'hmm_theme_shortcode_section');
add_shortcode('container', 'hmm_theme_shortcode_container');
add_shortcode('map', 'hmm_theme_shortcode_map');
add_shortcode('recette', 'hmm_theme_shortcode_recettes');
