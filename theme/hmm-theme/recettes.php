<?php
while (have_posts()) :
    $displayBanner = hmm_theme_page_banner();
    ?>
    <div class="container site-post">
        <?php query_posts('post_type= recette'); ?>
        <?php the_post(); ?>
        <?php if (!$displayBanner): ?>
            <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
        <?php endif; ?>
        <div class="post-content no-title">
            <?php the_content(); ?>
        </div>
        <div>
        </div>
    </div>
<?php endwhile; ?>
