<h1>Options du theme</h1>

<form action="options.php" method="POST">
    <?php settings_fields('hmm-theme'); ?>

    <table class="form-table">
        <tr>
            <th scope="row">
                <label for="hmm-maps-api-key">Clé d'API Google Maps</label>
            </th>
            <td>
                <input type="text" name="hmm-maps-api-key" id="hmm-maps-api-key"
                       value="<?php echo get_option('hmm-maps-api-key'); ?>" class="regular-text code">
            </td>
        </tr>
        <tr>
            <th scope="row">
                <label for="hmm-transparent-header">Bandeau transparent sur l&apos;accueil</label>
            </th>
            <td>
                <input type="checkbox" name="hmm-transparent-header" id="hmm-transparent-header"
                       <?php echo get_option('hmm-transparent-header') ? 'checked' : ''; ?>>
            </td>
        </tr>
        <tr>
            <th scope="row">
                <label for="hmm-bg-img">Change background image</label>
            </th>
            <td>
                <input id="hmm-bg-img" type="text" size="36" name="hmm-bg-img" value="<?= get_option('hmm-bg-img') ?>">
                <input id="contact_img_button" class="button" type="button" value="Choose picture">
            </td>
        </tr>
    </table>
    <p class="submit">
        <input type="submit" name="submit" id="submit" class="button button-primary" value="Enregistrer les modifications">
    </p>
</form>

