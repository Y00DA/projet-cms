<?php
/**
 * The template for displaying search results pages
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
get_header();
?>

<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
<input type="text" name="s" id="s" <?php if(is_search()) { ?>value="<?php the_search_query(); ?>" <?php } else { ?>value="Enter keywords &hellip;" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;"<?php } ?> /><br />
    
<?php $query_types = get_query_var('post_type'); ?>
    
<input type="checkbox" name="post_type[]" value="articles" <?php if (in_array('articles', $query_types)) { echo 'checked="checked"'; } ?> /><label>Articles</label>
<input type="checkbox" name="post_type[]" value="post" <?php if (in_array('post', $query_types)) { echo 'checked="checked"'; } ?> /><label>Blog</label>
<input type="checkbox" name="post_type[]" value="books" <?php if (in_array('books', $query_types)) { echo 'checked="checked"'; } ?> /><label>Books</label>
<input type="checkbox" name="post_type[]" value="videos" <?php if (in_array('videos', $query_types)) { echo 'checked="checked"'; } ?> /><label>Videos</label>
    
<input type="submit" id="searchsubmit" value="Search" />
</form>

<section id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

        <?php if (have_posts()) : ?>

            <header class="page-header">
                <h1 class="page-title"><?php printf(__('Search Results for: %s', 'hmm-theme'), '<span>' . esc_html(get_search_query()) . '</span>'); ?></h1>
            </header><!-- .page-header -->

            <?php
            // Start the loop.
            while (have_posts()) : the_post();

                /**
                 * Run the loop for the search to output the results.
                 * If you want to overload this in a child theme then include a file
                 * called content-search.php and that will be used instead.
                 */
                get_template_part('template-parts/content', 'search');

            // End the loop.
            endwhile;

            // Previous/next page navigation.
            the_posts_pagination(array(
                'prev_text' => __('Previous page', 'hmm-theme'),
                'next_text' => __('Next page', 'hmm-theme'),
                'before_page_number' => '<span class="meta-nav screen-reader-text">' . __('Page', 'hmm-theme') . ' </span>',
            ));

        // If no content, include the "No posts found" template.
        else :
            get_template_part('template-parts/content', 'none');

        endif;
        ?>

    </main><!-- .site-main -->
</section><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
