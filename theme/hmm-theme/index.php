<?php
get_header();
?>

<?php while (have_posts()) : ?>
    <div class="container site-post">
        <?php the_post(); ?>
        <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>

        <div class="post-content">
            <?php
            $excerpt = get_the_excerpt();
            $content = get_the_content();
            if ($excerpt != $content):
                ?>
                <?php the_excerpt(); ?>
                <a href="<?php the_permalink(); ?>">Lire la suite &hellip;</a>
            <?php else: ?>
                <?php the_content(); ?>
            <?php endif; ?>
        </div>
    </div>
<?php endwhile; ?>

<?php
get_footer();
