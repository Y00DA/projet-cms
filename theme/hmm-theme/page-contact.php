<?php
get_header();

$formValidated = false;
$formError = null;

if (isset($_POST['username']) && isset($_POST['mail']) && isset($_POST['message'])) {
    require get_template_directory() . '/function/mail.php';
    $formValidated = true;
    $formHandleResult = hmm_theme_handle_mail_form();

    if (isset($formHandleResult['error'])) {
        $formError = $formHandleResult['error'];
        $formValidated = false;
    }
}

while (have_posts()) :
    $displayBanner = hmm_theme_page_banner();
    ?>
    <div class="container site-post">
        <?php the_post(); ?>
        <?php if (!$displayBanner): ?>
            <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
        <?php endif; ?>
        <div class="post-content no-title">
            <?php if (!$formValidated): ?>
                <?php the_content(); ?>
            <?php endif;
            if ($formError):
                ?>
                <p><strong><?php echo $formError; ?></strong></p>
            <?php endif;
            if (!$formValidated):
                ?>
                <form method="post" action="/?page_id=<?php the_ID(); ?>" class="form-horizontal">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div>
                                <label class="placeholder" for="contact_name">Nom</label>
                                <input type="text" id="contact_name" name="username" required="required" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-12 visible-sm visible-xs"><br></div>
                        <div class="col-md-6 col-sm-12">
                            <div>
                                <label class="placeholder" for="contact_phone">Téléphone</label>
                                <input type="text" id="contact_phone" name="phone" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <label class="placeholder" for="contact_mail">E-Mail</label>
                                <input type="email" id="contact_mail" name="mail" required="required" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <label class="placeholder" for="contact_message">Message</label>
                                <textarea id="contact_message" name="message" required="required" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="submit">
                        <button type="submit" id="contact_submit" class="btn-default btn">Envoyer</button>
                    </div>
                </form>
            <?php else: ?>
                <p>Votre message à bien été envoyé.</p>
    <?php endif; ?>
        </div>
    </div>
<?php
endwhile;

get_footer();
